import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ITile } from '../types/tile';

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit {
  @Input() data: Observable<ITile[]>;
  constructor() { }

  ngOnInit() {
    
  }

}
