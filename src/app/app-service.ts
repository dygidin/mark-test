import { Injectable } from '@angular/core';
import * as json from '../assets/data.json';
import { Observable } from 'rxjs';
import { ITile } from './types/tile';
@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor() { }
  getData() {
    return new Observable<ITile[]>((observer) => {
      observer.next(json.data)
    })
  }
}
