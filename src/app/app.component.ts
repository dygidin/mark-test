import { Component } from '@angular/core';
import { AppService } from './app-service';
import { ITile } from './types/tile';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {  
  data: Observable<ITile[]> = this._appService.getData();
  constructor(public _appService: AppService) {}
}
