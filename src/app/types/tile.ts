export interface ITile {
  id: number,
  title: string;
  subtitle: string;
  lbs: string;
  description: string;
}